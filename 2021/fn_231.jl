module fn_231

function get_cost()
    return Dict("A" => 1, "B" => 10, "C" => 100, "D" => 1000)
end

function get_val_path()
return Dict(
    "H1"  => Dict("A1"   => ["H2", "H3"],
                   "B1"  => ["H2", "H3", "H4", "H5"],
                   "C1"  => ["H2", "H3", "H4", "H5", "H6", "H7"],
                   "D1"  => ["H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9"]),
     "H2"  => Dict("A1"  => ["H3"],
                   "B1"  => ["H3", "H4", "H5"],
                   "C1"  => ["H3", "H4", "H5", "H6", "H7"],
                   "D1"  => ["H3", "H4", "H5", "H6", "H7", "H8", "H9"]),
     "H4"  => Dict("A1"  => ["H3"],
                   "B1"  => ["H5"],
                   "C1"  => ["H5", "H6", "H7"],
                   "D1"  => ["H5", "H6", "H7", "H8", "H9"]),
     "H6"  => Dict("A1"  => ["H5", "H4", "H3"],
                   "B1"  => ["H5"],
                   "C1"  => ["H7"],
                   "D1"  => ["H7", "H8", "H9"]),
     "H8"  => Dict("A1"  => ["H7", "H6", "H5", "H4", "H3"],
                   "B1"  => ["H7", "H6", "H5"],
                   "C1"  => ["H7"],
                   "D1"  => ["H9"]),
     "H10" => Dict("A1"  => ["H9", "H8", "H7", "H6", "H5", "H4", "H3"],
                   "B1"  => ["H9", "H8", "H7", "H6", "H5"],
                   "C1"  => ["H9", "H8", "H7"],
                   "D1"  => ["H9"]),
     "H11" => Dict("A1"  => ["H10", "H9", "H8", "H7", "H6", "H5", "H4", "H3"],
                   "B1"  => ["H10", "H9", "H8", "H7", "H6", "H5"],
                   "C1"  => ["H10", "H9", "H8", "H7"],
                   "D1"  => ["H10", "H9"]),
     "A1"  => Dict("H1"  => ["H3", "H2"],
                   "H2"  => ["H3"],
                   "H4"  => ["H3"],
                   "H6"  => ["H3", "H4", "H5"],
                   "H8"  => ["H3", "H4", "H5", "H6", "H7"],
                   "H10" => ["H3", "H4", "H5", "H6", "H7", "H8", "H9"],
                   "H11" => ["H3", "H4", "H5", "H6", "H7", "H8", "H9", "H10"]),
     "B1"  => Dict("H1"  => ["H5", "H4", "H3", "H2"],
                   "H2"  => ["H5", "H4", "H3"],
                   "H4"  => ["H5"],
                   "H6"  => ["H5"],
                   "H8"  => ["H5", "H6", "H7"],
                   "H10" => ["H5", "H6", "H7", "H8", "H9"],
                   "H11" => ["H5", "H6", "H7", "H8", "H9", "H10"]),
     "C1"  => Dict("H1"  => ["H7", "H6", "H5", "H4", "H3", "H2"],
                   "H2"  => ["H7", "H6", "H5", "H4", "H3"],
                   "H4"  => ["H7", "H6", "H5"],
                   "H6"  => ["H7"],
                   "H8"  => ["H7"],
                   "H10" => ["H7", "H8", "H9"],
                   "H11" => ["H7", "H8", "H9", "H10"]),
     "D1"  => Dict("H1"  => ["H9", "H8", "H7", "H6", "H5", "H4", "H3", "H2"],
                   "H2"  => ["H9", "H8", "H7", "H6", "H5", "H4", "H3"],
                   "H4"  => ["H9", "H8", "H7", "H6", "H5"],
                   "H6"  => ["H9", "H8", "H7"],
                   "H8"  => ["H9"],
                   "H10" => ["H9"],
                   "H11" => ["H9", "H10"]))
end

function get_room_status_t1()
return Dict(
    "H1"  => [],
    "H2"  => [],
    "H3"  => [],
    "H4"  => [],
    "H5"  => [],
    "H6"  => [],
    "H7"  => [],
    "H8"  => [],
    "H9"  => [],
    "H10" => [],
    "H11" => [],
    "A1"  => ["A", "A"],
    "B1"  => ["B", "B"],
    "C1"  => ["D", "D"],
    "D1"  => ["C", "C"])
end

function get_room_status_t2()
return Dict(
    "H1"  => [],
    "H2"  => [],
    "H3"  => [],
    "H4"  => [],
    "H5"  => [],
    "H6"  => [],
    "H7"  => [],
    "H8"  => [],
    "H9"  => [],
    "H10" => [],
    "H11" => [],
    "A1"  => ["B", "D", "D", "A"],
    "B1"  => ["C", "C", "B", "D"],
    "C1"  => ["B", "B", "A", "C"],
    "D1"  => ["D", "A", "C", "A"])
end

function get_room_status_i1()
return Dict(
    "H1"  => [],
    "H2"  => [],
    "H3"  => [],
    "H4"  => [],
    "H5"  => [],
    "H6"  => [],
    "H7"  => [],
    "H8"  => [],
    "H9"  => [],
    "H10" => [],
    "H11" => [],
    "A1"  => ["B", "D"],
    "B1"  => ["B", "C"],
    "C1"  => ["C", "A"],
    "D1"  => ["D", "A"])
end

function get_room_status_i2()
return Dict(
    "H1"  => [],
    "H2"  => [],
    "H3"  => [],
    "H4"  => [],
    "H5"  => [],
    "H6"  => [],
    "H7"  => [],
    "H8"  => [],
    "H9"  => [],
    "H10" => [],
    "H11" => [],
    "A1"  => ["B", "D", "D", "D"],
    "B1"  => ["B", "C", "B", "C"],
    "C1"  => ["C", "B", "A", "A"],
    "D1"  => ["D", "A", "C", "A"])
end

function chk_valid(dict)
    is_valid = true
    for (key, val) in dict
        if (startswith(key, "H") && length(val) > 0) || !all(startswith.(key, val))
            is_valid = false
            break
        end
    end
    
    return is_valid
end

function solve(room, path, costs, cnt)
    cost = [1000000]
    
    for (key, val) in room
        if length(val) > 0 && !all(startswith.(key, val))
            for (key_2, val_2) in path[key]
                through = all([length(room[x]) == 0 for x in val_2])
                dest    = (startswith(key_2, "H") && length(room[key_2]) == 0) || (startswith(key_2, val[1]) && all(room[key_2] .== val[1]))
                if through && dest
                    room_new = deepcopy(room)
                    val_out  = popfirst!(room_new[key])
                    push!(room_new[key_2], val_out)
                    
                    steps     = length(val_2) + (startswith(key, "H") ? cnt - length(room[key_2]) : cnt - length(room[key]) + 1)
                    cost_this = steps * costs[val_out]

                    if !chk_valid(room_new)
                        cost_this += solve(room_new, path, costs, cnt)
                    end
                    
                    push!(cost, cost_this)
                end
            end
        end
    end
    
    return minimum(cost)
end

end