module fn_181

function parse_val(vec_in, level = 1)
    if isa(vec_in[1], Vector)
        v_l = parse_val(vec_in[1], level + 1)
    else
        v_l = vec_in[1]
    end

    if isa(vec_in[2], Vector)
        v_r = parse_val(vec_in[2], level + 1)
    else
        v_r = vec_in[2]
    end

    return Dict("val_left" => v_l, "val_right" => v_r, "level" => level)
end

function nice(dict_in)
    if isa(dict_in["val_left"], Dict)
        v_l = nice(dict_in["val_left"])
    else
        v_l = dict_in["val_left"]
    end

    if isa(dict_in["val_right"], Dict)
        v_r = nice(dict_in["val_right"])
    else
        v_r = dict_in["val_right"]
    end

    return "[$v_l,$v_r]"
end

function rup(val, adt)
    if adt == 0
        res = val
    else
        res = val
        if isa(val, Dict)
            res["val_right"] = rup(res["val_right"], adt)
        else
            res = val + adt
        end
    end
    
    return res    
end

function lup(val, adt)
    if adt == 0
        res = val
    else
        res = val
        if isa(val, Dict)
            res["val_left"] = lup(res["val_left"], adt)
        else
            res = val + adt
        end
    end
    
    return res    
end

function lvlup(val)
    res = copy(val)
    res["level"] += 1
    
    if isa(res["val_left"], Dict)
        res["val_left"] = lvlup(res["val_left"])
    end
    
    if isa(res["val_right"], Dict)
        res["val_right"] = lvlup(res["val_right"])
    end
    
    return res    
end

function reduce_val(dict_in)
    if dict_in["level"] == 5
        dict_out = Dict("left"    => dict_in["val_left"],
                        "right"   => dict_in["val_right"],
                        "value"   => 0,
                        "reduced" => true)
    else
        v_l = dict_in["val_left"]
        v_r = dict_in["val_right"]
        
        if isa(v_l, Dict)
            v_dl = reduce_val(v_l)
            
            o_l  = v_dl["left"]
            r_l  = v_dl["reduced"]
            
            v_l  = v_dl["value"]
            v_r  = lup(v_r, v_dl["right"])
        else
            o_l  = 0
            r_l  = false
        end
        
        if isa(v_r, Dict)
            v_dr = reduce_val(v_r)
            
            o_r  = v_dr["right"]
            r_r  = v_dr["reduced"]
            
            v_l  = rup(v_l, v_dr["left"])
            v_r  = v_dr["value"]
        else
            o_r  = 0
            r_r  = false
        end
        
        dict_out = Dict("left"    => o_l,
                        "right"   => o_r,
                        "value"   => Dict("val_left"  => v_l,
                                          "val_right" => v_r,
                                          "level"     => dict_in["level"]),
                        "reduced" => r_l || r_r)
    end
    
    return dict_out
end

function split_val(dict_in, has_split = false)
    if isa(dict_in["val_left"], Dict)
        o_l = split_val(dict_in["val_left"])
        v_l = o_l["value"]
        r_l = o_l["split"]
    else
        if dict_in["val_left"] > 9
            v_l = Dict("val_left"  => Int(floor(dict_in["val_left"] / 2)),
                       "val_right" => Int(ceil(dict_in["val_left"] / 2)),
                       "level"     => dict_in["level"] + 1)
            r_l = true
        else
            v_l = dict_in["val_left"]
            r_l = false
        end
    end

    if !r_l && isa(dict_in["val_right"], Dict)
        o_r = split_val(dict_in["val_right"])
        v_r = o_r["value"]
        r_r = o_r["split"]
    else
        if !r_l && dict_in["val_right"] > 9
            v_r = Dict("val_left"  => Int(floor(dict_in["val_right"] / 2)),
                       "val_right" => Int(ceil(dict_in["val_right"] / 2)),
                       "level"     => dict_in["level"] + 1)
            r_r = true
        else
            v_r = dict_in["val_right"]
            r_r = false
        end
    end

    dict_out = Dict("value" => Dict("val_left"  => v_l,
                                    "val_right" => v_r,
                                    "level"     => dict_in["level"]),
                    "split" => r_l || r_r)
    return dict_out
end

function sum_val(v_l, v_r)
    dict_out = Dict("val_left"  => lvlup(v_l),
                    "val_right" => lvlup(v_r),
                    "level"     => 1)
    
    while true
        dict_rd  = reduce_val(dict_out)
        dict_vl  = split_val(dict_rd["value"])
        dict_out = dict_vl["value"]
        
        if !dict_rd["reduced"] && !dict_vl["split"]
            break
        end
    end
    
    return dict_out
end

function magnitude(dict_in)
    
    if isa(dict_in["val_left"], Dict)
        v_l = magnitude(dict_in["val_left"])
    else
        v_l = dict_in["val_left"]
    end
    
    if isa(dict_in["val_right"], Dict)
        v_r = magnitude(dict_in["val_right"])
    else
        v_r = dict_in["val_right"]
    end
    
    return 3 * v_l + 2 * v_r
end

end